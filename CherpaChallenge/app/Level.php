<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';
    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('enabled');
    }
}
