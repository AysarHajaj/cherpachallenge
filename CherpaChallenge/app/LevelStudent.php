<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelStudent extends Model
{
    protected $table = 'level_student';
}
