<?php

namespace App\Http\Controllers;
use App\Course;
use App\Classroom;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Get api data
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request){
        //get the courseId from request body
        $courseId = $request->course_id;
        //get classroomId from request body
        $classroomId = $request->classroom_id;
        //get the course by its Id
        $course = Course::find($courseId);
        //get classroom by its Id
        $classroom = Classroom::find($classroomId);
        //get All stages for the course and the level of each stage
        $courseStagesAndLevels = $this->courseStagesAndLevels($course);
        //get stages and levels
        $stagesArray = $courseStagesAndLevels["stagdesAndLevels"];
        //get all levels belongs to this course
        $levelsArray = $courseStagesAndLevels["allLevels"];
        //get students in this classroom
        $students = $classroom->students;
        //get students with enables levels
        $allStudentsEnabledLevels = $this->allStudentsEnabledLevels($students, $levelsArray);
        return response()->json(array("stages" => $stagesArray, "studentsLevels" => $allStudentsEnabledLevels));
    }

    /**
     * Get the stages of the course and the levels foreach stage
     * @param  \Course  $course
     * @return \Array
     */
    public function courseStagesAndLevels($course){
        $stagesArray = [];
        $allLevelsArray = [];
        foreach ($course->stages as $stage) {
            $levelsArray = [];
            foreach ($stage->levels as $level) {
                $levelName = $level->name;
                array_push($levelsArray, $levelName);
                array_push($allLevelsArray, $level->id);
            }
            $stageName = $stage->name;
            array_push($stagesArray, array("stageName" => $stageName, "levels" => $levelsArray));
        }
        return array("stagdesAndLevels" => $stagesArray, "allLevels" => $allLevelsArray);
    }

    /**
     * Get students and their enabled levels
     * @param  \Array  $students
     * @param  \Array  $allLevelsArray
     * @return \Array
     */
    public function allStudentsEnabledLevels($students, $allLevelsArray){
        $studentLevels = [];
        foreach ($students as $student) {
            $levels = $this->studentEnabledLevels($student, $allLevelsArray);
            $studentName = $student->name;
            array_push($studentLevels, array("studentName" => $studentName, "enabledLevels" => $levels));
        }
        return $studentLevels;
    }

    /**
     * Get the stages of the course and the levels foreach stage
     * @param  \Student  $student
     * @param  \Array  $allLevelsArray
     * @return \Array
     */
    public function studentEnabledLevels($student, $allLevelsArray){
        $levelsNameArray = [];
        foreach ($student->levels as $level) {
            if($level->pivot->enabled){
                if(in_array($level->id, $allLevelsArray)){
                    array_push($levelsNameArray, $level->name);
                }
            }
        }
        return $levelsNameArray; 
    }
}
