<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    public function classrooms()
    {
        return $this->belongsToMany('App\Classroom');
    }

    public function levels()
    {
        return $this->belongsToMany('App\Level')->withPivot('enabled');;
    }
}
