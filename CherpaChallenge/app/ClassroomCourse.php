<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassroomCourse extends Model
{
    protected $table = 'classroom_course';
}
