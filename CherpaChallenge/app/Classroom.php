<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $table = 'classrooms';
    public function students()
    {
        return $this->belongsToMany('App\Student');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course');
    }
}
