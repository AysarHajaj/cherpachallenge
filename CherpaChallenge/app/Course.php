<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    public function stages()
    {
        return $this->hasMany('App\Stage');
    }

    public function classrooms()
    {
        return $this->belongsToMany('App\Classroom');
    }
}
