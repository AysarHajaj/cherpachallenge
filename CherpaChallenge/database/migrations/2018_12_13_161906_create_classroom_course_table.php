<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_course', function (Blueprint $table) {
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('classroom_id');
            $table->primary(['course_id', 'classroom_id']);
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classroom_course', function (Blueprint $table) {
            $table->dropForeign(['course_id']);
            $table->dropForeign(['classroom_id']);
        });

        Schema::dropIfExists('classroom_course');
    }
}
