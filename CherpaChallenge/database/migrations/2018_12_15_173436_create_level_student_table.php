<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_student', function (Blueprint $table) {
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('level_id');
            $table->boolean('enabled');
            $table->primary(['student_id', 'level_id']);
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('level_student', function (Blueprint $table) {
            $table->dropForeign(['student_id']);
            $table->dropForeign(['level_id']);
        });
        Schema::dropIfExists('level_student');
    }
}
