<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => "aysar", 'email' => "aysarhajaj@gmail.com", "password" => '$2y$10$gBO3Kt4UV/Ni6FqiPJCziuwTrjRDlg4sob6k4etQA.zsVsODSmQ0u']
        ];
        
        User::insert($users);
    }
}
