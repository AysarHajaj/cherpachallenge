<?php

use Illuminate\Database\Seeder;
use App\Classroom;

class ClassroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classrooms = [
            ['name' => 'Class One'],
            ['name' => 'Class Two'],
            ['name' => 'Class Three']
        ];
        
        Classroom::insert($classrooms);
    }
}
