<?php

use Illuminate\Database\Seeder;
use App\ClassroomCourse;

class ClassroomsCoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classroomsCourses = [
            ['course_id' => 1, 'classroom_id' => 1],
            ['course_id' => 1, 'classroom_id' => 2],
            ['course_id' => 1, 'classroom_id' => 3],
            ['course_id' => 2, 'classroom_id' => 1],
            ['course_id' => 2, 'classroom_id' => 2],
            
        ];
        
        ClassroomCourse::insert($classroomsCourses);
    }
}
