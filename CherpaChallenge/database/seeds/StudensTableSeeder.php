<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = [
            ['name' => 'Aysar'],
            ['name' => 'Wissam'],
            ['name' => 'Elia'],
            ['name' => 'Zeinab'],
            ['name' => 'Nadine'],
            ['name' => 'Ahmad']
        ];
        
        Student::insert($students);
    }
}
