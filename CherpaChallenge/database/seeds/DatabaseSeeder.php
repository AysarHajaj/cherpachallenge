<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CoursesTableSeeder::class,
            StagesTableSeeder::class,
            LevelsTableSeeder::class,
            StudensTableSeeder::class,
            ClassroomsTableSeeder::class,
            ClassroomsStudentsTableSeeder::class,
            ClassroomsCoursesTableSeeder::class,
            LevelsStudentsTableSeeder::class,
            UsersTableSeeder::class
        ]);
    }
}
