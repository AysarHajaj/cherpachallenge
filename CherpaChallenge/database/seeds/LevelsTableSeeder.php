<?php

use Illuminate\Database\Seeder;
use App\Level;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            ['name' => 'Level One', 'stage_id' => 1],
            ['name' => 'Level Two', 'stage_id' => 1],
            ['name' => 'Level Three', 'stage_id' => 2],
            ['name' => 'Level Four', 'stage_id' => 2],
            ['name' => 'Level Five', 'stage_id' => 3],
            ['name' => 'Level Six', 'stage_id' => 3],
            ['name' => 'Level Seven', 'stage_id' => 4],
            ['name' => 'Level Eight', 'stage_id' => 4],
            ['name' => 'Level Nine', 'stage_id' => 5],
        ];
        
        Level::insert($levels);
    }
}
