<?php

use Illuminate\Database\Seeder;
use App\LevelStudent;

class LevelsStudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levelsStudednts = [
            ['student_id' => 1, 'level_id' => 1, "enabled" => true],
            ['student_id' => 1, 'level_id' => 2, "enabled" => true],
            ['student_id' => 1, 'level_id' => 3, "enabled" => false],
            ['student_id' => 1, 'level_id' => 4, "enabled" => true],
            ['student_id' => 2, 'level_id' => 1, "enabled" => true],
            ['student_id' => 2, 'level_id' => 2, "enabled" => true],
            ['student_id' => 2, 'level_id' => 3, "enabled" => false],
            ['student_id' => 2, 'level_id' => 4, "enabled" => true],
        ];
        
        LevelStudent::insert($levelsStudednts);
    }
}
