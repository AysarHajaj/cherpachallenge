<?php

use Illuminate\Database\Seeder;
use App\Course;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = [
            ['name' => 'Java'],
            ['name' => 'C#'],
            ['name' => 'PHP']
        ];
        
        Course::insert($courses);
    }
}
