<?php

use Illuminate\Database\Seeder;
Use App\ClassroomStudent;

class ClassroomsStudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classroomsStudednts = [
            ['student_id' => 1, 'classroom_id' => 1],
            ['student_id' => 1, 'classroom_id' => 2],
            ['student_id' => 1, 'classroom_id' => 3],
            ['student_id' => 2, 'classroom_id' => 1],
            ['student_id' => 2, 'classroom_id' => 2],
            
        ];
        
        ClassroomStudent::insert($classroomsStudednts);
    }
}
