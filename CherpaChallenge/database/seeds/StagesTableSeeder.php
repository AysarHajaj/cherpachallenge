<?php

use Illuminate\Database\Seeder;
use App\Stage;

class StagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stages = [
            ['name' => 'one', 'course_id' => 1],
            ['name' => 'two', 'course_id' => 1],
            ['name' => 'three', 'course_id' => 1],
            ['name' => 'four', 'course_id' => 2],
            ['name' => 'five', 'course_id' => 2],
            ['name' => 'six', 'course_id' => 2],
            ['name' => 'seven', 'course_id' => 3],
            ['name' => 'eight', 'course_id' => 3],
            ['name' => 'nine', 'course_id' => 3],
        ];
        
        Stage::insert($stages);
    }
}
